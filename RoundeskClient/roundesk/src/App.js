import './App.css';
import { Route, Routes } from 'react-router-dom'
import Register from './pages/register/Register';
import Login from './pages/login/Login';
import Home from './pages/home/Home';
import Acceuil from './pages/acceuil/Acceuil';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { getCurrentUser} from './features/authSlice';
import AddAgent from './pages/agents/AddAgent';
import AgentList from './pages/agents/AgentList';
import EditAgent from './pages/agents/EditAgent';
function App() {
  const dispatch=useDispatch()
  const {user}=useSelector((state)=>state.auth)
  
  useEffect(()=>{
    dispatch(getCurrentUser())},
  [dispatch])
  return (
    <>
    <Routes>
   <>
   {user.username? <>
    <Route path='/home'  element={<Home />}>
      <Route index element={<Acceuil/>}></Route>
      <Route path='/home/addagent' element={<AddAgent/>}></Route>
      <Route path='/home/agentslist' element={<AgentList/>}></Route>
      <Route path='/home/agentdetails/:id' element={<EditAgent/>}></Route>
    </Route>
    
    </>
    :<>
    <Route path='/login' element={<Login/>}/>
    <Route path='/' element={<Register/>}/>
    </>
    }
      
    
      </> 

    
   
    </Routes>
    </>
  );
}

export default App;
