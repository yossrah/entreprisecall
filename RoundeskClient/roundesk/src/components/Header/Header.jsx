import React from 'react'
import '@fortawesome/fontawesome-free/css/all.min.css';
import LoginPic from '../../images/logoroundesk.png'
import { useSelector } from 'react-redux';
const Header = () => {
  const {user}=useSelector((state)=>state.auth)
  return (
    <nav className="navbar navbar-light bg-white">
      <div className="container-fluid">
        <a className="navbar-brand d-flex align-items-center mb-3 mb-md-0 me-md-auto text-black text-decoration-none" href="#">
        <img 
          src={LoginPic} style={{width:"40%", height:"40%"}}
      />
        </a>
        <form className="d-flex align-items-start mb-3 mb-md-0 me-md-auto" style={{ width: '500px' }} role="search">
        
        <input className="form-control me-2 bg-light" type="search"
       // value={destinationgroup}
        //onChange={handleDestinationGroupChange} 
        placeholder="Search" aria-label="Search"
        />
       
        
      </form>
        <div className="dropdown">
         
          <i className="fas fa-bell" style={{ marginRight: '16px' }}></i>
          <i className="fas fa-user" style={{ marginRight: '16px' }}></i>
          <button className="btn btn-light dropdown-toggle" type="button" id="dropdownMenu1" data-bs-toggle="dropdown" aria-expanded="false">
          {user.username}
          </button>
        </div>
      </div>
    </nav>
  )
}

export default Header
