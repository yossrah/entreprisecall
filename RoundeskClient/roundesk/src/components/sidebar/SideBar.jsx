import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import { logoutUser } from '../../features/authSlice'

function getItemStyle(selectedPage, item) {

    if (selectedPage === item) {
      return "nav-link active d-flex justify-content-center align-items-center"
    }
    return "nav-link text-black d-flex justify-content-center align-items-center"
  }
  
const SideBar = ({selectedPage}) => {
    const dispatch=useDispatch()
    const navigate=useNavigate()
    const logout=()=>{
        dispatch(logoutUser({navigate}))
    }
  return (
    <div className="d-flex flex-column flex-shrink-0 p-3 text-black bg-white h-100" style={{backgroundColor:'white'}}>
      <ul className="nav nav-pills flex-column mb-auto">
        <li className="nav-item my-2">
        </li>
        <li className="nav-item my-2">
          <Link to="/home/addagent" 
          className={getItemStyle(selectedPage,"/dashboard")}
          >
          Ajouter nouvel agent
          </Link>
        </li>
        <li className="nav-item my-2">
        <Link to="/home/agentslist" 
        className={getItemStyle(selectedPage,"/graduation")}
        >
            Liste Totale des agents
          </Link>
        </li>
      </ul>
      <hr />
      <div className="dropdown">
        <a href="#" className="d-flex align-items-center text-black text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
          <img src="https://picsum.photos/200" alt="" width="32" height="32" className="rounded-circle me-2" />
          <strong onClick={logout}>Deconnexion</strong>
        </a>
      </div>
    </div>
  )
}

export default SideBar
