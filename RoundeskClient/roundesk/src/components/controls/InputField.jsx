import React, { useState } from 'react'
import { Form, Button, InputGroup } from 'react-bootstrap';
import { EyeFill, EyeSlashFill, GeoAltFill,EnvelopeFill } from 'react-bootstrap-icons';

const InputField = ({ name, label, onChangeHandler, errors, value, type, autoFocus, errorsFront }) => {
    const [showPassword, setShowPassword] = useState(false);
      
        const toggleShowPassword = () => {
          setShowPassword(!showPassword);
        };
  return (
    <>
    <Form.Group className="mb-3" controlId={name}>
    <div style={{ textAlign: 'left' }}>
    </div>
    <InputGroup>
      <Form.Control
        required
        type={showPassword ? 'text' : type}
        name={name}
        value={value}
        onChange={onChangeHandler}
        isInvalid={!!errors ||!! errorsFront}
        autoFocus={autoFocus}
        placeholder={label}
      />
      {type === 'password' && (
        <Button variant="outline-secondary" onClick={toggleShowPassword}>
          {showPassword ? <EyeSlashFill /> : <EyeFill />}
        </Button>
      )}
      <Form.Control.Feedback type="invalid">
        {errors} {errorsFront}
      </Form.Control.Feedback>
    </InputGroup>
  </Form.Group>
    </>
  )
}

export default InputField
