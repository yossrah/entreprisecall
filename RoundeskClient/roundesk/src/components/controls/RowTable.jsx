import React from 'react'

const RowTable = ({key,name,phone,email,destination_group,date,authorized,index,photo,handleDelete,handleUpdate}) => {
  const formatDate = (isoString) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(isoString).toLocaleDateString(undefined, options);
  }
   console.log(`Image URL: http://localhost:3001/uploads/${photo}`);
  return (
    <>
    <tr key={key}>
    
    {name?<td>{name}</td>:null}
    {phone?<td>{phone}</td>:null}
    {email?<td>{email}</td>:null}
    
    {destination_group?<td>{destination_group}</td>:null}
    {date?<td>{formatDate(date)}</td>:null}
    <td>
    <button type="button"  className="btn btn-danger " onClick={handleDelete}>Delete</button>
    <button type="button" className="btn btn-success " style={{backgroundColor:'#33DB37' }}  onClick={handleUpdate}>Edit</button>
    </td>
  </tr>
    </>
  )
    
}

export default RowTable
