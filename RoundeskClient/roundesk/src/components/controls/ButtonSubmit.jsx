const ButtonSubmit = ({type,title}) => {
    return (
        <>
        <div 
        
        className="text-end"
        >
           <button type={type}
            className="btn btn-success "
            style={{backgroundColor:'#33DB37', border: 'none' }} 
            >{title}</button>
            </div>
      </>    
    )
  }
  
  export default ButtonSubmit