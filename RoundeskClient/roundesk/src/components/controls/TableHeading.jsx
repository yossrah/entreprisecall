import React from 'react'

const TableHeading = ({name, phone,email,destination_group,date,authorisation}) => {
  return (

    <>
    <thead className="">
    <tr>
      {name?<th>{name}</th>:null}
      {phone?<th>{phone}</th>:null}
      {destination_group?<th>{destination_group}</th>:null}
      {email?<th>{email}</th>:null}
      
     
      {authorisation?<th>{authorisation}</th>:null}
      {date?<th>{date}</th>:null}
      <th>Action</th>
    </tr>
  </thead>
    </>
  )
}

export default TableHeading