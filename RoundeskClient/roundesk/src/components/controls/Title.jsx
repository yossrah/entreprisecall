import React from 'react'

const Title = ({ caption, title, color,name }) => {
  return (
    <div className="mb-3">
      <h4 style={{ color: color }}>
        <h3>{title} {name}</h3>
      </h4>
      <p className="text-muted">{caption}</p>
    </div>
  )
}

export default Title
