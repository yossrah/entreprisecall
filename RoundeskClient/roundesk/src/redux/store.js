import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../features/authSlice"
import agentReducer from "../features/agentSlice"
export const store = configureStore({
    reducer: {
        auth:authReducer,
        agent:agentReducer

    },
    
})