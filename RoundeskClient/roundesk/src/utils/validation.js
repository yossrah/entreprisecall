import { useState } from "react";

    export const validate = (form,setErrors) => {
        let isValid = true;
        let errors = {};
    
        if (!form.username) {
            errors.username = 'Username is required';
            isValid = false;
        }
    
        if (!form.password) {
            errors.password = 'Password is required';
            isValid = false;
        }
    
        if (form.password !== form.confirme_password) {
            errors.confirme_password = 'Passwords do not match';
            isValid = false;
        }
    
        setErrors(errors);
        return isValid;

}


