import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import Swal from 'sweetalert2'

const initialState = {
  agents: [],
  agent:{},
  loading: 'false',
  error: null,
  currentPage: 1,
  destinationgroup: '',
}
export const fetchAgents = createAsyncThunk(
    'agents/fetchAgents',
    async ({ currentPage = 1, destinationgroup }) => {
        const accessToken = localStorage.getItem('jwt');
        if (!accessToken) {
          throw new Error('No access token found');
        }
      const response = await axios.get('http://localhost:3001/apiagences/agents/',{
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },}, {
        params: { currentPage, destinationgroup },
      });
      return response.data;
    }
  );

  export const fetchAgentsByName = createAsyncThunk(
    'agents/fetchAgentsByName',
    async ({name}  ,{ rejectWithValue }) => {
     try{
      console.log('Agzzzzzzzzzzzzzznts',name)
      const accessToken = localStorage.getItem('jwt');
        if (!accessToken) {
          throw new Error('No access token found');
        }
      const response = await axios.get('http://localhost:3001/apiagences/agents/name', {
        params: { name },},{
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },});
      return response.data;
     }
     catch (error) {
      Swal.fire({
        title: 'Error!',
        text: 'Something went wrong! try again',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
      return rejectWithValue(error.response.data);
    } 
    }
  );

  export const deleteAgent = createAsyncThunk(
    'agents/deleteAgent',
    async (id, { rejectWithValue }) => {
      try {
        const accessToken = localStorage.getItem('jwt');
        if (!accessToken) {
          throw new Error('No access token found');
        }
        const response = await axios.delete(`http://localhost:3001/apiagences/agents/${id}`,{
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },});
        return response.data;
      } catch (error) {
        return rejectWithValue(error.response.data);
      }
    }
  );


  export const updateAgent = createAsyncThunk('agents/updateAgent',
    async ( {id, form,navigate}, { rejectWithValue }) => {
      console.log("iiiiiiiidiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii",id)
      try {
        const accessToken = localStorage.getItem('jwt');
        if (!accessToken) {
          throw new Error('No access token found');
        }
        const response = await axios.put(`http://localhost:3001/apiagences/agents/${id}`, form,
            {
                headers: {
                  Authorization: `Bearer ${accessToken}`,
                },}
        );
          const Toast = Swal.mixin({
            toast: true,
            position: 'center-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
          Toast.fire({
            icon: 'success',
            title: 'Updated  successfully'
          })
           navigate('/home/agentslist');
          return response.data;
      } catch (error) {
        Swal.fire({
          title: 'Error!',
          text: 'Something went wrong! try again',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
       // console.log("i-------------------------------------------------",id)
        return rejectWithValue(error.response.data);
      }
    }
  );
  
  export const fetchAgent = createAsyncThunk(
    'agents/fetchAgent',
    async (id, { rejectWithValue }) => {
      try {
        const accessToken = localStorage.getItem('jwt');
        if (!accessToken) {
          throw new Error('No access token found');
        }
        const response = await axios.get(`http://localhost:3001/apiagences/agents/${id}`,
            {
                headers: {
                  Authorization: `Bearer ${accessToken}`,
                },}
        );
        return response.data;
      } catch (error) {
        return rejectWithValue(error.response.data);
      }
    }
  );

  export const createAgent = createAsyncThunk(
    'agents/createAgent',
    async ({ form, navigate }, { rejectWithValue }) => {
      console.log('-----------------------94------------',{form})
      try {
        const accessToken = localStorage.getItem('jwt');
        if (!accessToken) {
          throw new Error('No access token found');
        }
        const response = await axios.post('http://localhost:3001/apiagences/agents', form,
            {
                headers: {
                  Authorization: `Bearer ${accessToken}`,
                },}
        );
        
        console.log('-----------------------96------------',form)
        const Toast = Swal.mixin({
          toast: true,
          position: 'center-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer);
            toast.addEventListener('mouseleave', Swal.resumeTimer);
          },
        });
  
        Toast.fire({
          icon: 'success',
          title: 'Agent created successfully',
        });
  
        navigate('/home/agentslist');
  
        return response.data;
      } catch (error) {
        console.error("Error creating agent:", error.message);
  
        Swal.fire({
          title: 'Error!',
          text: 'Something went wrong! Please try again.',
          icon: 'error',
          confirmButtonText: 'Ok',
        });
  
        return rejectWithValue(error.response.data);
      }
    }
  );
  

  const agentsSlice = createSlice({
    name: 'agents',
    initialState,
    reducers: {
      setCurrentPage(state, action) {
        state.currentPage = action.payload;
      },
      setDestinationGroup(state, action) {
        state.destinationgroup = action.payload;
      },
    },
    extraReducers: (builder) => {
      builder
        .addCase(fetchAgents.pending, (state) => {
          state.loading = true;
        })
        .addCase(fetchAgents.fulfilled, (state, action) => {
          state.loading = false;
          state.agents = action.payload;
        })
        .addCase(fetchAgents.rejected, (state, action) => {
          state.loading = false;
          state.error = action.error.message;
        })

        .addCase(deleteAgent.pending, (state) => {
          state.loading = true;
          state.error = null;
        })
        .addCase(deleteAgent.fulfilled, (state, action) => {
          state.loading = false;
          state.agents = state.agents.filter(agent => agent.id !== action.meta.arg);
        })
        .addCase(deleteAgent.rejected, (state, action) => {
          state.loading = false;
          state.error = action.payload;
        })

        .addCase(fetchAgent.pending, (state) => {
          state.loading = true;
          state.error = null;
        })
        .addCase(fetchAgent.fulfilled, (state, action) => {
          state.loading = false;
          state.agent = action.payload;
        })
        .addCase(fetchAgent.rejected, (state, action) => {
          state.loading = false;
          state.error = action.payload;
        })

        .addCase(updateAgent.pending, (state) => {
          state.loading = true;
          state.error = null;
        })
        .addCase(updateAgent.fulfilled, (state, action) => {
          state.loading = false;
          state.agent = action.payload;
        })
        .addCase(updateAgent.rejected, (state, action) => {
          state.loading = false;
          state.error = action.payload;
        })

        .addCase(createAgent.pending, (state) => {
          state.loading = true;
        })
        .addCase(createAgent.fulfilled, (state, action) => {
          state.loading = false;
          state.agent = action.payload;
        })
        .addCase(createAgent.rejected, (state, action) => {
          state.loading = false;
          state.error = action.payload.error?action.payload.error:action.payload.message ;
        })

        .addCase(fetchAgentsByName.pending, (state) => {
          state.loading = true;
        })
        .addCase(fetchAgentsByName.fulfilled, (state, action) => {
          state.status = false;
          state.agents = action.payload;
        })
        .addCase(fetchAgentsByName.rejected, (state, action) => {
          state.status = false;
          state.error = action.payload.error;
        });
    },
  });
  
  export const { setCurrentPage, setDestinationGroup } = agentsSlice.actions;
  
  export default agentsSlice.reducer;