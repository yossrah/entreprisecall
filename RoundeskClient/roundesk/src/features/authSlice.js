import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import Swal from 'sweetalert2'


const initialState = {
    isConnected:false,
    loading: false,
    user:{},
    accessToken:null,
    error: null,
   };

   export const registerUser = createAsyncThunk('users/registerUser', 
    async ({ form, navigate }, { rejectWithValue }) => {
      try {
        const response = await axios.post('http://localhost:3001/apiagences/auth', form);
        navigate('/login');
        return response.data;
      } catch (error) {
        console.error("Error registering user:", error.message);
        return rejectWithValue(error.response.data);
      }
    }
  );

  export const loginUser = createAsyncThunk('users/loginUser', 
    async ({ form, navigate }, { rejectWithValue, dispatch  }) => {
      try {
        const response = await axios.post('http://localhost:3001/apiagences/auth/login', form);
        const {accessToken}=response.data
        localStorage.setItem('jwt',accessToken)
        console.log('accessToken: '+accessToken)
        // const decode=jwtDecode(accessToken)
        // console.log('decodeeeeeeeeeee',decode)
        // response.payload=decode
          const Toast = Swal.mixin({
          toast: true,
          position: 'center-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Connected  successfully'
        })
        navigate('/home');
        return response.data;
      } catch (error) {
        console.error("Error registering user:", error.message);
        Swal.fire({
          title: 'Error!',
          text: 'Something went wrong! try again',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
        return rejectWithValue(error.response.data);
      }
    }
  );

  export const getCurrentUser = createAsyncThunk(
    'users/getCurrentUser',
    async (_, { rejectWithValue }) => {
      try {
        const accessToken = localStorage.getItem('jwt');
        if (!accessToken) {
          throw new Error('No access token found');
        }
        const response = await axios.get('http://localhost:3001/apiagences/auth/me', {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
        return response.data;
      } catch (error) {
        console.error("Error getting current user:", error.message);
        return rejectWithValue(error.response?.data || { message: error.message });
      }
    }
  );

  export const logoutUser=createAsyncThunk('users/logoutUser',async({ navigate })=>{
    localStorage.removeItem("jwt")
    navigate('/login')
  })

  export const authSlice= createSlice({
    name:'auth',
    initialState,
    reducers:{},
    extraReducers: (builder) => {
        builder
          .addCase(registerUser.pending, (state) => {
            state.loading = true;
            state.error=null;
          })
          .addCase(registerUser.fulfilled, (state, action) => {
            state.loading = false;
            state.user = action.payload;
          })
          .addCase(registerUser.rejected, (state, action) => {
            state.loading = false;
            //action.payload.message 
            state.error=action.payload.error?action.payload.error:action.payload.message ;
          })

          .addCase(loginUser.pending, (state) => {
            state.loading = true;
            state.error=null;
            state.user={}
          })
          .addCase(loginUser.fulfilled, (state, action) => {
            state.loading = false;
            state.user = action.payload.user;
            state.accessToken=action.payload.accessToken;
          })
          .addCase(loginUser.rejected, (state, action) => {
            state.loading = false;
            //action.payload.message 
            state.error=action.payload.error ;
          })

          .addCase(getCurrentUser.pending, (state) => {
            state.loading = true;
            state.error=null;
            state.user={};
            
          })
          .addCase(getCurrentUser.fulfilled, (state, action) => {
            state.loading = false;
            state.user = action.payload;
            state.accessToken=action.payload.accessToken;
          })
          .addCase(getCurrentUser.rejected, (state, action) => {
            state.loading = false;
            //action.payload.message 
            state.error=action.payload.error ;
          })

          .addCase(logoutUser.fulfilled, (state, action) => {
            state.loading = false;
            state.user = {};
            state.accessToken={};
          })
      }
  })
  export default authSlice.reducer;
