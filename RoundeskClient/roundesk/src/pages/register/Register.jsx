import React, { useState } from 'react'
import InputField from '../../components/controls/InputField'
import Title from '../../components/controls/Title'
import ButtonSubmit from '../../components/controls/ButtonSubmit'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { registerUser } from '../../features/authSlice'
import { validate } from '../../utils/validation'
import LoginPic from '../../images/logoroundesk.png'
const Register = () => {
    const [form,setForm]=useState({})
    //const [errorConfirm, setErrorConfirm] = useState('');
    const [errors, setErrors] = useState({
      username: '',
      password: '',
      confirme_password:''
  });

    const navigate=useNavigate()
    const error=useSelector((state)=>state.auth.error)
    const dispatch=useDispatch()
    const onChangeHandler=(e)=>{
        setForm({ ...form,[e.target.name]:e.target.value})
        
       }
       const onSubmit=(e)=>{
        e.preventDefault()
        if(validate(form,setErrors)){
          console.log("formSubmitted",form)
          dispatch(registerUser({ form, navigate }))
          .then(response => {
            console.log("RegisterUser response:", response);
        }).catch(error => {
          console.error("RegisterUser error:", error);
        });
        }
        
        // if(form.password===form.confirme_password){
        //   const { confirm, ...formWithoutConfirm } = form;
        //   console.log("formSubmitted",form)
        //   dispatch(registerUser({ form:formWithoutConfirm, navigate }))
        //   .then(response => {
        //     console.log("RegisterUser response:", response);
        // }).catch(error => {
        //   console.error("RegisterUser error:", error);
        // });
        // }
        // else{
        //   setErrorConfirm("Passwords do not match");
        // }
      }
  return (
    <>
    <div className="container-fluid h-100">
    <div className="row h-100">
    <div className="col-12 col-sm-6 col-md-6 d-none d-sm-block" style={{
      //backgroundImage: `url(${LoginPic})`,
      //backgroundColor:'green',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '100vh',
    }}>
    <div className="" style={{
      backgroundImage: `url(${LoginPic})`,
      //backgroundColor:'green',
      backgroundSize: '70%',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '100vh',
    }}></div>
    </div>
      <div className="col-12 col-sm-8 col-md-6 d-flex flex-column bg-light justify-content-center align-items-center p-4" style={{ backgroundColor: 'white' }}>
        <div className="my-5 text-center" style={{ width: '80%' }}>
          <Title title="Roundesk Technologies" caption="Bienvenue chez notre Start-UP"></Title>
          <form noValidate 
          onSubmit={onSubmit} 
          className="needs-validation">
          <div className="mb-3">
          <InputField  name="username" label="Email" type="text" 
           onChangeHandler={onChangeHandler} errors={error?.username} errorsFront={errors?.username} 
            autoFocus="autofocus" />
          <InputField  name="password" label="nouveau mot de passe" type="password"  
          onChangeHandler={onChangeHandler} errors={error?.password} errorsFront={errors?.password} 
           ></InputField>
           
          <InputField  name="confirme_password" label="Confirmer le mot de passe" errors={error?.confirme_password} errorsFront={errors?.confirme_password} type="password" 
         onChangeHandler={onChangeHandler} />
          </div>
          <ButtonSubmit type="submit" title="inscrire" ></ButtonSubmit>
          </form>
        </div>
      </div>
    </div>
  </div>
    </>
  )
}

export default Register
