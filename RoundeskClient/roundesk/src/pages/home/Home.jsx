import React from 'react'
import Header from '../../components/Header/Header'
import SideBar from '../../components/sidebar/SideBar'
import { Outlet } from 'react-router-dom'
import Acceuil from '../acceuil/Acceuil'

const Home = ({user}) => {
  return (
    <>
    <Header></Header>
    <div className="container-fluid h-100">
        <div className="row h-100">
          <div className="col-2 p-0">
              <SideBar></SideBar>
      </div>
        <div className= "col-10 p-0 bg-light h-100" >
           <Outlet/>
        </div>
      </div>
      </div> 
      </>
  )
}

export default Home
