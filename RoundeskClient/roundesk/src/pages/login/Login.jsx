import React, { useState } from 'react'
import Title from '../../components/controls/Title'
import InputField from '../../components/controls/InputField'
import ButtonSubmit from '../../components/controls/ButtonSubmit'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loginUser } from '../../features/authSlice'
import LoginPic from '../../images/logoroundesk.png'
const Login = () => {
  const [form,setForm]=useState({})
    const navigate=useNavigate()
    const error=useSelector((state)=>state.auth.error)
    const dispatch=useDispatch()
    const onChangeHandler=(e)=>{
        setForm({ ...form,[e.target.name]:e.target.value})
        
       }

       const [errors, setErrors] = useState({
        username: '',
        password: '',
    });

    const validate = () => {
      let isValid = true;
      let errors = {};

      if (!form.username) {
          errors.username = 'Username is required';
          isValid = false;
      }

      if (!form.password) {
          errors.password = 'Password is required';
          isValid = false;
      }

      // if (form.password !== formData.confirmPassword) {
      //     errors.confirmPassword = 'Passwords do not match';
      //     isValid = false;
      // }

      setErrors(errors);
      return isValid;
  };

       const onSubmit=(e)=>{
        e.preventDefault()
        if (validate()) {
          // Handle form submission
          console.log('Form submitted successfully', form);
          dispatch(loginUser({ form, navigate }))
          .then(response => {
            console.log("RegisterUser response:", response);
        })
        .catch(error => {
          console.error("RegisterUser error:", error);
        });
      }
      
  }


  return (
    <>
    <div className=" container-fluid h-100 ">
    <div className="row h-100">
    <div className="col-12 col-sm-6 col-md-6 d-none d-sm-block" style={{
      //backgroundImage: `url(${LoginPic})`,
      //backgroundColor:'green',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '100vh',
    }}>
    <div className="" style={{
      backgroundImage: `url(${LoginPic})`,
      //backgroundColor:'green',
      backgroundSize: '70%',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '100vh',
    }}></div>
    </div>
      <div className="col-12 col-sm-8 col-md-6 d-flex flex-column bg-light justify-content-center align-items-center p-4 " 
     // style={{ backgroundColor:'#202221' }}
      >
        <div className="my-5 text-center" style={{ width: '70%' }}>
          <Title title="Roundesk Technologies" caption="Bienvenue chez notre Start-UP"></Title>
          <form noValidate 
          onSubmit={onSubmit} 
          className="needs-validation">
          <div className="mb-3">
          
          <InputField  name="username" label="Email" type="text" 
          onChangeHandler={onChangeHandler} errors={error?.username}
            autoFocus="autofocus" />
            {errors.username && <span className="error">{errors.username}</span>}
          <InputField  name="password" label="nouveau mot de passe" type="password"  
          onChangeHandler={onChangeHandler} errors={error?.password}
           ></InputField>
            {errors.password && <span className="error">{errors.password}</span>}
           
           
   <div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"/>
  <label class="form-check-label" for="flexCheckDefault">Mot de passes oublié?</label></div>
  </div>
          
          <ButtonSubmit type="submit" title="connexion" ></ButtonSubmit>
          </form>
        </div>
      </div>
    </div>
  </div>
    </>
  )
}

export default Login
