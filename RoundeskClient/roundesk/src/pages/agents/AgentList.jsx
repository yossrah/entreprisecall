import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2'
import { deleteAgent, fetchAgents, fetchAgentsByName, setCurrentPage, setDestinationGroup } from '../../features/agentSlice';
import Title from '../../components/controls/Title';
import TableHeading from '../../components/controls/TableHeading';
import RowTable from '../../components/controls/RowTable';
const AgentsList = () => {
  const navigate=useNavigate()
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const {loading,agents,agent,error,currentPage,destinationgroup}=useSelector((state) => state.agent);
  useEffect(() => {
    dispatch(fetchAgents({ currentPage, destinationgroup }));
  }, [dispatch, currentPage, destinationgroup,navigate]);

  const handlePageChange = (page) => {
    dispatch(setCurrentPage(page));
  };

  const handleDestinationGroupChange = (event) => {
    dispatch(setDestinationGroup(event.target.value));
  };
  const handleFetchAgent = (name) => {
    name = name.toLowerCase()
    dispatch(fetchAgentsByName({name}));
  };
  const users=[]

  // console.log('agentssss',name)
  //   const filteredUsers = users.filter(user =>
  //     user.username.toLowerCase().includes(searchTerm.toLowerCase())
  //   );
  // console.log("Authooooooooooooooo",agents.map(agent=>agent.authorized))
  const HandleDelete=(_id)=>{
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Deleted!',
          'user has been deleted.',
          'success'
        )
        dispatch(deleteAgent(_id))
      }
    })
    }
  return (
    <>
    {loading == true && <div>Loading...</div>}
    {loading == false && <div>{error}</div>}
    
    <div className="container mt-5 bg-white" style={{ border: '1px solid #dee2e6', borderRadius: '10px', margin: '20px', padding: '20px' }}>
    <Title title="Tous les agents"></Title>
    
      <div className="container mt-3 mb-4">
      <div className="row justify-content-center">
        <div className="col-6">
        <div className="input-group">
    </div>
      </div>
      </div>
      <div className="container mt-3 mb-4">
      <input
        type="text"
        className="form-control"
        value={destinationgroup}
        onChange={handleDestinationGroupChange}
        placeholder="Filter by Destination Group"
      /></div>
      </div>
    <table className="table caption-top ">
    <TableHeading name="Nom de l'agent" index="Index" email="Email" destination_group="Group destination" 
     phone="Numéro"  date="Created At"/>
      <tbody>
        {agents?.map((agent,index) => (
          <RowTable key={agent.id}
          index={index}
          name={agent.name} 
          email={agent.email} 
          phone={agent.phone}
          authorized={agent.authorized}
          destination_group={agent.destination_group}
          date={agent.CreatedAt}
          handleDelete={()=>HandleDelete(agent.id)}
          handleUpdate={()=>navigate(`/home/agentdetails/${agent.id}`)}
          />
        ))}
      </tbody>
    </table>
    <div class="d-flex justify-content-center">
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <li class="page-item">
                <button class="page-link" onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1}>
                    Previous
                </button>
            </li>
            <li class="page-item">
                <button class="page-link" disabled>
                    {currentPage}
                </button>
            </li>
            <li class="page-item">
                <button class="page-link" onClick={() => handlePageChange(currentPage + 1)}>
                    Next
                </button>
            </li>
        </ul>
    </nav>
</div>
  </div>
    </>
  )
}

export default AgentsList
