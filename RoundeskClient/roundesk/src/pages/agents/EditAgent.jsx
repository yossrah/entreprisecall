import React, { useEffect, useState } from 'react'
import InputField from '../../components/controls/InputField'
import Title from '../../components/controls/Title'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { fetchAgent, updateAgent } from '../../features/agentSlice'

const EditAgent = () => {
    const dispatch=useDispatch()
  const navigate=useNavigate()
  const {id}=useParams()
  const {agent}=useSelector((state)=>state.agent)
  const [form,setForm]=useState({})
     const onChangeHandler=(e)=>{
      setForm({
       ...form,[e.target.name]:e.target.value
      })
      }
        
     const onSubmit=(e)=>{
       e.preventDefault()
     // console.log('submit tttttttttttttttt', form,id)
      dispatch(updateAgent({id,form,navigate}))
     
    }
    useEffect(() => {
      dispatch(fetchAgent(id));
    }, []);
    useEffect(() => {
      if(agent){
        setForm({...agent})
      }
    }, [agent]);
   // console.log("fooooooo
  return (
    <>
    {form?.authorized}
    <div className="container-fluid h-100">
    <div className="row h-100">
    <div className="col-12 col-sm-6 col-md-6 d-none d-sm-block" style={{
      //backgroundImage: `url(${LoginPic})`,
      //backgroundColor:'green',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '100vh',
    }}>
    
    <div className="" style={{
      //backgroundImage: `url(${LoginPic})`,
      //backgroundColor:'green',
      backgroundSize: '70%',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '100vh',
    }}>
    
    </div>
    
    </div>
      <div className="col-12 col-sm-8 col-md-6 d-flex flex-column bg-light justify-content-center align-items-center p-4" style={{ backgroundColor: 'white' }}>
        <div className="my-5 text-center" style={{ width: '80%' }}>
          <Title title="Les données de Mr" name={form?.name} ></Title>
          <form noValidate 
          onSubmit={onSubmit} 
          className="needs-validation">
          <div className="mb-3">
          <InputField  name="name" value={form.name} label="Nom de l'agent" type="text" 
             onChangeHandler={onChangeHandler} 
             ></InputField>
             <h6 style={{textAlign:'left'}}>Numéro de téléphone</h6>
            <InputField  name="phone" label="Entrez le numéro de téléphone" value={form.phone}  onChangeHandler={onChangeHandler} type="text"   />
            <h6 style={{textAlign:'left'}}>Email</h6>
            <InputField  name="email" value={form.email} label="Entrez l'adresse email" type="text" 
            onChangeHandler={onChangeHandler} 
               ></InputField>
               <h6 style={{textAlign:'left'}}>Groupe de destination</h6>
            <InputField  name="destination_group" label="Entrez le groupe de destination" value={form.destination_group}  onChangeHandler={onChangeHandler} type="text" />
            <h6 style={{textAlign:'left'}}>Autorisé à émettre des appels</h6>
            <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button"  onClick={()=>{ setForm({ ...form, authorized: true });}} class="btn btn-light">Oui</button>
            <button type="button" class="btn btn-light" onClick={()=>{ setForm({ ...form, authorized: false });}}>Non</button>
     
         </div>
          </div>
          <button type="button" class="btn btn-light">Annuler</button>
          <button type="submit" style={{backgroundColor:'#33DB37', border: 'none' }}  class="btn btn-success">Modifier</button>
          </form>
        </div>
      </div>
    </div>
  </div>
    </>
  )
}

export default EditAgent
