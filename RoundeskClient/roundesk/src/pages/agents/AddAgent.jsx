import React, { useState } from 'react'
import InputField from '../../components/controls/InputField'
import ButtonSubmit from '../../components/controls/ButtonSubmit'
import Title from '../../components/controls/Title'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { createAgent } from '../../features/agentSlice'
import ProfilePic from '../../images/profile.png'
import PlusPic from '../../images/plus.png'
const AddAgent = () => {
  const [form,setForm]=useState({})
  const [file,setFile]=useState(null)
    const navigate=useNavigate()
    const error=useSelector((state)=>state.agent.error)
    const dispatch=useDispatch()
    const onChangeHandler=(e)=>{
         setForm({ ...form,[e.target.name]:e.target.value})
        
       }
      //  const onChangeImage=(e)=>{
      //   if(e.target.files[0]){
      //     setFile(e.target.files[0])
      //     console.log("fffffffffff",file)
      //   }
        
      //   }

       const onSubmit=(e)=>{
        console.log('ffffffffffff in onsubmit',form)
        e.preventDefault();
        dispatch(createAgent({form,navigate})).then(response => {
          console.log("RegisterUser response:", response);
      })
      .catch(error => {
        console.error("RegisterUser error:", error);
      });
      }
      //console.log("foorm",form)
      //console.log('fiiiiiiiiiiiiiiiiiile seted',file)
  return (
    <>
    {form?.authorized}
    <div className="container-fluid h-100">
    <div className="row h-100">
    <div className="col-12 col-sm-6 col-md-6 d-none d-sm-block" style={{
      //backgroundImage: `url(${LoginPic})`,
      //backgroundColor:'green',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '100vh',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    }}>

    <div className="" style={{
     // backgroundImage: `url(${PlusPic})`,
      //backgroundColor:'green',
      backgroundSize: '30px',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '30vh',
      marginTop: '20px',
      
    }}>
    
    </div>
    
    <div className="" style={{
      backgroundImage: `url(${ProfilePic})`,
      //backgroundColor:'green',
      backgroundSize: '30%',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      height: '40vh',
    }}>
    
    </div>
    
    </div>
      <div className="col-12 col-sm-8 col-md-6 d-flex flex-column bg-light justify-content-center align-items-center p-4" style={{ backgroundColor: 'white' }}>
        <div className="my-5 text-center" style={{ width: '80%' }}>
          <Title title="Ajouter les données d'un agent"></Title>
          <form noValidate 
          onSubmit={onSubmit} 
          className="needs-validation">
          <div className="mb-3">
          <InputField  name="name" value={form.name} label="Nom de l'agent" type="text" 
             onChangeHandler={onChangeHandler} 
             ></InputField>
             <h6 style={{textAlign:'left'}}>Numéro de téléphone</h6>
            <InputField  name="phone" label="Entrez le numéro de téléphone" value={form.phone}  onChangeHandler={onChangeHandler} type="text"   />
            <h6 style={{textAlign:'left'}}>Email</h6>
            <InputField  name="email" value={form.email} label="Entrez l'adresse email" type="text" 
            onChangeHandler={onChangeHandler} 
               ></InputField>
               <h6 style={{textAlign:'left'}}>Groupe de destination</h6>
            <InputField  name="destination_group" label="Entrez le groupe de destination" value={form.destination_group}  onChangeHandler={onChangeHandler} type="text" />
            <h6 style={{textAlign:'left'}}>Autorisé à émettre des appels</h6>
            <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button"  onClick={()=>{ setForm({ ...form, authorized: true });}} class="btn btn-light">Oui</button>
            <button type="button" class="btn btn-light" onClick={()=>{ setForm({ ...form, authorized: false });}}>Non</button>
     
         </div>
          </div>
          <button type="button" class="btn btn-light">Annuler</button>
          <button type="submit" style={{backgroundColor:'#33DB37', border: 'none' }}  class="btn btn-success">inscrire</button>
          </form>
        </div>
      </div>
    </div>
  </div>
    </>
  )
}

export default AddAgent
