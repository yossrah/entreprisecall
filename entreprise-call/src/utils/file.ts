import { diskStorage } from "multer"

export const fileStorage={
    
    storage:diskStorage({
        destination:'./uploads/photos',
        filename:(req,file,cb)=>{
          cb(null,file.originalname)
        }
      })
}