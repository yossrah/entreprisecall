import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModuleAsyncOptions, TypeOrmModuleOptions } from "@nestjs/typeorm";
import { Agent } from "src/entities/agent.entity";
import { User } from "src/entities/user.entity";

export default class TypeOrmConfig {
    static getOrmConfig(configService: ConfigService): TypeOrmModuleOptions {
        const typeOrmConfig: TypeOrmModuleOptions = {
            type: 'mysql',
            host: process.env.DB_HOST,
            port: Number(process.env.MYsql_PORT),
            username: String(configService.get('DB_USERNAME'))  ,
            password: configService.get('PASSWORD') ,
            database: configService.get('DATABASE'),
            entities: [User,Agent],
            synchronize: Boolean(configService.get('TYPEORM_SYNC'))
        }
        return typeOrmConfig;
    }
}

export const typeOrmConfigAsync: TypeOrmModuleAsyncOptions = {
    imports: [ConfigModule],
    useFactory: async (configservice: ConfigService): Promise<TypeOrmModuleOptions> => TypeOrmConfig.getOrmConfig(configservice),
    inject: [ConfigService]
}