import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/entities/user.entity';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';

@Module({
  imports:[ ConfigModule.forRoot(),
    //provide the strategy
  PassportModule.register({defaultStrategy:'jwt'}),
  //jwtModule setup
  JwtModule.register({
    //secret key to sign the payload for jwt token
    secretOrPrivateKey: process.env.ACESS_SECRET,
    signOptions: { expiresIn: '3600s' },
  }),

    TypeOrmModule.forFeature([User])
  ],
  controllers: [AuthController],
  providers: [AuthService,
    ConfigService,
    JwtStrategy
  ],
  exports:[
    JwtStrategy,
    PassportModule
  ]
})
export class AuthModule {}
