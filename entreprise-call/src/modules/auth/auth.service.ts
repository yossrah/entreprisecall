import { ConflictException, Injectable, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entities/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt'
import { AuthCredentialsDto } from './dtos/authCredentials.dto';
import { SignInCredentialsDto } from './dtos/signInCredentials.dto';
import { JwtPayload } from 'src/common/interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {
    constructor(@InjectRepository(User) private readonly userRepository:Repository<User>,
    private readonly jwtService:JwtService
    ){}

    private async hashPassword(password: string, salt: string): Promise<string> {//return Promise<string> because it is asyn funct
        return bcrypt.hash(password, salt)
      }

      private async validatePassword(password:string,userpassword:string){
        return await bcrypt.compare(password, userpassword);
      }

      async register(createUserDto:AuthCredentialsDto):Promise<User>{
        const {username}=createUserDto
        try{
          const existedUser=await this.userRepository.findOneBy({username})
          if(existedUser){
            throw new ConflictException('Username already exists');
          }
        //generate salt 
        const salt=await bcrypt.genSalt()
        //console.log(salt)
        const newUser = this.userRepository.create({
          ...createUserDto,
          role:'Admin',
          CreatedAt: new Date(),
        })
        //hash user password
        newUser.password= await this.hashPassword(newUser.password, salt)
        //save user to db
        return await this.userRepository.save(newUser);
      }
        catch(error){
            if (error instanceof ConflictException) {
                throw error; // Rethrow conflict exception
              } 
              else {
                // Handle other exceptions (e.g., database error)
                throw new InternalServerErrorException('Failed to create user');
              }
        }
}

   async validateUser({username,password}:SignInCredentialsDto){
    const existingUser = await this.userRepository.findOneBy({ username });
    if (!existingUser) {
      throw new ConflictException('unvalid credentials');
     }
// Compare the provided password with the hashed password stored in the database
   const isPasswordValid =await this.validatePassword(password, existingUser.password);
   if (!isPasswordValid) {
     throw new UnauthorizedException('Invalid credentials');
  }
  //extract the payload structure into an interface
  const payload:JwtPayload= { username: existingUser.username, id: existingUser.id, role: existingUser.role };
  const accessToken = await this.jwtService.sign(payload);
  return {accessToken,
          user: existingUser  }
  }

  async findOneById(id: number): Promise<User | undefined> {
    const user= await this.userRepository.findOneBy({id});
    return user
  }
}
