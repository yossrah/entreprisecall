import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dtos/authCredentials.dto';
import { User } from 'src/entities/user.entity';
import { SignInCredentialsDto } from './dtos/signInCredentials.dto';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guards';
import { get } from 'http';
import { GetUser } from 'src/common/decorators/get-user.decorator';

@Controller('auth')
export class AuthController {

    constructor(private readonly authService:AuthService ){}
    @Post('')
    register(@Body() createUserDto:AuthCredentialsDto):Promise<User>{
            return this.authService.register(createUserDto)
        }
    
   @Post('login')
    login(@Body() SignInPayload:SignInCredentialsDto){
         return this.authService.validateUser(SignInPayload)
    }

  @UseGuards(JwtAuthGuard)
 
  @Get('me')
  getProfile(@GetUser() user:User) {
    return user;
  }
}
