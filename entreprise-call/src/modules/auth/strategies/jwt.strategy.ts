import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import { JwtPayload } from "src/common/interfaces/jwt-payload.interface";
import { User } from "src/entities/user.entity";
import { Repository } from "typeorm";
import { Strategy, ExtractJwt } from 'passport-jwt';

import { AuthService } from "../auth.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
    constructor(@InjectRepository(User) private readonly userRepository:Repository<User>,
    private readonly userService: AuthService){
        super({
        //retrieve the jwt token from the request
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        ignoreExpiration: false,
        secretOrKey: process.env.ACESS_SECRET})
    }

    async validate(payload: JwtPayload): Promise<User> {
        console.log('inside validateeeeeeeeeee',payload)
        const id = payload.id
        console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiid',id)
        const user = await this.userService.findOneById( id )
        if (!user) {
            console.log('inside validateeeeeeeeeee',payload)
            throw new UnauthorizedException();
        }
        console.log('ussssssssssssssssssssssser',user)
        return user;
    }

}