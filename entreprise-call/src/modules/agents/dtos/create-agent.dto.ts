import { IsEmail, IsInt, IsNotEmpty } from "class-validator"

export class CreateAgentDto {

    @IsNotEmpty()
    name:string

    @IsEmail()
    email:string

    @IsNotEmpty()
    phone: number

    authorized:boolean


    @IsNotEmpty()
    destination_group:string

    photo:string
}