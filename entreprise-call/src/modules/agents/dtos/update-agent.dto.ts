import { CreateAgentDto } from "./create-agent.dto";
import { PartialType } from '@nestjs/mapped-types';
export class UpdateAgentDto extends PartialType(CreateAgentDto) {}