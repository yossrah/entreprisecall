import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AgentsService } from './agents.service';
import { UpdateAgentDto } from './dtos/update-agent.dto';
import { Agent } from 'src/entities/agent.entity';
import { CreateAgentDto } from './dtos/create-agent.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { fileStorage } from 'src/utils/file';
//import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/common/decorators/roles.decorator';
import { Role } from 'src/common/enums/role.enum';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guards';
import { RolesGuard } from 'src/common/guards/roles.guards';
import { UserInterceptor } from 'src/common/interceptors/user.interceptors';
import { AuthGuard } from '@nestjs/passport';

@Controller('agents')

@UseGuards(JwtAuthGuard)
//@Roles(Role.Admin||Role.Responsable)
//@UseGuards(RolesGuard)
export class AgentsController {

    constructor(private readonly agentsService: AgentsService) {}

  @Post()
  create(@Body() CreateAgentDto: CreateAgentDto ):Promise<Agent> {
    return this.agentsService.create(CreateAgentDto);
  }


  @Get()
  
  @UseInterceptors(UserInterceptor)
  findAll(@Query('destinationgroup') destinationgroup:string, @Query('currentPage') currentPage:number=1):Promise<Agent []> {
    return this.agentsService.findAll(currentPage,destinationgroup);
  }

  @Get('/name')
  findByName(@Query('name') name: string): Promise<Agent[]> {
    return this.agentsService.findByName(name);
  }

  @Get(':id')
  findOne(@Param('id',ParseIntPipe) id: number):Promise<Agent> {
    return this.agentsService.findOne(id);
  }



  @Put(':id')
  update(@Param('id',ParseIntPipe) id: number, @Body() updateAgentDto: UpdateAgentDto ) {
    return this.agentsService.update(id, updateAgentDto);
  }

  @Delete(':id')
  remove(@Param('id',ParseIntPipe) id: number) {
    return this.agentsService.remove(id);
  }

}
