import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Agent } from 'src/entities/agent.entity';
import { Repository } from 'typeorm';
import { CreateAgentDto } from './dtos/create-agent.dto';
import { UpdateAgentDto } from './dtos/update-agent.dto';

@Injectable()
export class AgentsService {
    constructor(@InjectRepository(Agent) private readonly agentRepository: Repository<Agent>,){}
  async create(createAgentDto: CreateAgentDto,):Promise<Agent>
   {
    const {email}=createAgentDto
    const existEmail = await this.agentRepository.findOneBy( {email})
    if (existEmail) {
      throw new HttpException('Agent already exist', HttpStatus.BAD_REQUEST)
    }
    const new_agent=this.agentRepository.create({...createAgentDto,
        CreatedAt: new Date(),

    })
    return await this.agentRepository.save(new_agent)
  }

  async findAll(currentPage:number,destinationgroup?:string ): Promise<Agent[]> {
    const resPerPage = 5
    const skip = resPerPage * (currentPage - 1)
    const queryBuilder = this.agentRepository.createQueryBuilder('agent');

    if (destinationgroup) {
      queryBuilder.andWhere('agent.destination_group = :destinationgroup', { destinationgroup });
    }
    queryBuilder.orderBy('agent.createdAt', 'DESC').skip(skip).take(resPerPage);

    return queryBuilder.getMany();
  }

  async findOne(id: number): Promise<Agent> {
    const agent= await this.agentRepository.findOneBy({id});
    if(!agent){
        throw new HttpException('Agent not found', HttpStatus.NOT_FOUND)
    }
    return agent;
  }

  async findByName(name: string): Promise<Agent[]> {
    console.log('naaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaame',name)
    const agents = await this.agentRepository.find({
      where: {
        name: name,
      },
    });

    if (!agents || agents.length === 0) {
      throw new NotFoundException(`Agents with name '${name}' not found`);
    }

    return agents;
  }

  async update(id: number, updateAgentDto: UpdateAgentDto) {
    const agent= await this.agentRepository.findOneBy({id});
    if(!agent){
        throw new HttpException('Agent not found', HttpStatus.NOT_FOUND)
    }
    return await this.agentRepository.update(id, updateAgentDto)
  }

  
  async remove(id: number) {
    return await this.agentRepository.delete({id})
  }
}
