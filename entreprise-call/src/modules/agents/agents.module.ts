import { Module } from '@nestjs/common';
import { AgentsService } from './agents.service';
import { AgentsController } from './agents.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Agent } from 'src/entities/agent.entity';
import { PassportModule } from '@nestjs/passport';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports:[TypeOrmModule.forFeature([Agent]),
  PassportModule,
  AuthModule,],
  providers: [AgentsService],
  controllers: [AgentsController]
})
export class AgentsModule {}
