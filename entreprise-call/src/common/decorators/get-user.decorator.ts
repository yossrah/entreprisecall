import { ExecutionContext, createParamDecorator } from "@nestjs/common";
import { User } from "src/entities/user.entity";

//whatever we return from this function is going to be set to the parameter that is decorator with this decorator
export const GetUser= createParamDecorator((data:unknown,ctx: ExecutionContext):User=>{
    const req=ctx.switchToHttp().getRequest();
    return req.user
})