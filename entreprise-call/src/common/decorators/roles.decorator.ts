import { SetMetadata } from "@nestjs/common";
import { Role } from "../enums/role.enum";

//create a roles decorator
//In the above code, the key is roles, and the value is passed from the roles argument. The saved metadata can be used by the role guard later.


export const Roles = (...roles: Role[]) => SetMetadata('roles', roles);