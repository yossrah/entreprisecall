import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { plainToInstance } from "class-transformer";
import { Observable, map } from "rxjs";
import { User } from "src/entities/user.entity";

@Injectable()
export class UserInterceptor implements NestInterceptor{
    intercept(context: ExecutionContext, next: CallHandler<User[]>): Observable<any> | Promise<Observable<any>> {
        //console.log(context.getClass().name)
        //pass to the request handler
        return next.handle()
          .pipe(map((data)=>data.map((user)=>plainToInstance(User,user))))  
           //return an observable
           //intercept the response
    }
}