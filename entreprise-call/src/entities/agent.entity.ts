import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:"agents"})
export class Agent extends BaseEntity{

  @PrimaryGeneratedColumn({type:"bigint"})
  id:number

  @Column({ type: 'varchar', length: 30})
  name: string;

  @Column({ type: 'varchar', length: 30})
  email: string;

  @Column()
  phone: number;

  @Column({default:true})
  authorized: Boolean;

  @Column({ type: 'varchar', length: 30})
  destination_group: string;

  @Column({ type: 'varchar',nullable:true })
  photo: string;

  @Column()
  CreatedAt:Date



}