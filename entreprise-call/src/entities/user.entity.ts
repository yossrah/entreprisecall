import { Exclude } from "class-transformer";
import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:"users"})
export class User extends BaseEntity{

  @PrimaryGeneratedColumn({type:"bigint"})
  id:number

  @Column({ type: 'varchar', length: 30})
  username: string;

  @Exclude()
  @Column({ type: 'varchar' })
  password: string;

  @Column()
  CreatedAt:Date

  @Column()
  role:string

  
}